$(document).ready(function() {
	$.ajax({
        url: "/lemenu/templates/background_template",             
        success: function(data) {
            var obj = JSON.parse(data);
            for(var index in obj){
	            option = "<option value="+index+">"+obj[index]+"</option>";
	            $("#select_background_box").append(option);
            }   
        }
    });
});

$("#select_background_box").change(function() {
	var state = $('#select_background_box').val();
	$("body").css('background-image', 'url(/lemenu/app/assets/img/background/'+state+'/'+state+'.png)');
});

$("#option_font_box").change(function() {
	var font_name = $('#option_font_box').val();
	$(".font_tamplate, .option_font_title_size_box, .option_font_category_size_box").css('font-family', font_name);
});

$("#option_font_title_size_box").change(function() {
	var font_size = $('#option_font_title_size_box').val();
	$(".option_font_title_size_box").css({fontSize: font_size+'px', display: "block"});
});

$("#option_font_category_size_box").change(function() {
	var font_size = $('#option_font_category_size_box').val();
	$(".option_font_category_size_box").css({fontSize: font_size+'px', position: "relative", marginTop: "20px"});
});

$("#option_font_size_cardapio_box").change(function() {
	var font_size = $('#option_font_size_cardapio_box').val();
	$(".item_title").css({fontSize: font_size+'px'});
	$(".text_description").css({fontSize: font_size+'px'});
});

$("#option_font_cardapio_box").change(function() {
	var font_name = $('#option_font_cardapio_box').val();
	console.log(font_name);
	$(".item_title").css('font-family', font_name);
	$(".text_description").css('font-family', font_name);
});

$("#option_menu_box").change(function() {
	var columns = $('#option_menu_box').val();
	if (columns == 1) {
		var column_class = 'col-md-12';
	} else if (columns == 2){
		var column_class = 'col-md-6';
	} else if (columns == 3){
		var column_class = 'col-md-4';
	}
	console.log(column_class);
	$(".menu_template_columns").removeClass('col-md-4 col-md-6 col-md-12');
	$(".menu_template_columns").addClass(column_class);
});

