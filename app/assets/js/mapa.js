var map;
var latlng;
var InfoBoxAberto;
var infoBox = [];

function initialize() {
    var estabelecimento;
    $.ajax({
        dataType: "json",
        url: "/lemenu/maps",                    
        success: function(data) {
            var address = data.logradouro + ',' + data.numero + data.cidade + ',' + data.cep;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
                latlng = new google.maps.LatLng(lat, lng);
                carregar_endereco(data, lat, lng);
              } else {
                 alert("Não foi possivel obter localização: " + status);
              }
            });
         
            var options = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
         
            map = new google.maps.Map(document.getElementById("mapa"), options);
        }
    });
}


function carregar_endereco(estabelecimento, latitude, longitude){
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        title: estabelecimento.nome,
        map: map,
        icon: '/lemenu/app/assets/img/red-icon-map.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: estabelecimento.nome
      });
    
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    var content = "<p>Estamos aberto "+estabelecimento.horario_funcionamento+"</p>";
    content += "<span>Aceitamos "+estabelecimento.formas_pagamentos+"</span>";
    content += "<span>Cardápio: <a href=\"#\">"+estabelecimento.nome+"</a></span>";
    var myOptions = {
        content: content,
        pixelOffset: new google.maps.Size(-150, 0)
    };
 
    infoBox = new InfoBox(myOptions);
    infoBox.marker = marker;
 
    infoBox.listener = google.maps.event.addListener(marker, 'click', function (e) {
        abrirInfoBox(marker);
    });
}

function abrirInfoBox(marker) {
    if (typeof(InfoBoxAberto) == true && typeof(infoBox) == 'object') {
        infoBox[InfoBoxAberto].close();
    }
 
    infoBox.open(map, marker);
    InfoBoxAberto = true;
}

initialize();

//start of modal google map
$('#mapModal').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, "resize");
    map.setCenter(latlng);
});