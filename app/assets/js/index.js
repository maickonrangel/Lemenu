

$("#estado").change(function(){
	var id_state = $('#estado').val();
    var url = "/lemenu/estabelecimentos/get_city/"+id_state;
    $.ajax({url: url, success: function(result){
        var obj = JSON.parse(result);
        $("#cidade").empty();
        $("#cidade").append("<select value=\"\">Selecione</select>");
        if (obj.id == 882){
            option = "<option value="+obj.id+">"+obj.nome+"</option>";
            $("#cidade").append(option);
        } else {
            for(var prop in obj){
                option = "<option value="+obj[prop].id+">"+obj[prop].nome+"</option>";
                $("#cidade").append(option);
            }
        }

        $("#cidade").selectpicker("refresh");
    }});
});

$("#select-horario-funcionamento").change(function() {
    var option = $("#select-horario-funcionamento").val();
    if (option == 3) {
        $("#form-horario-funcionamento").fadeIn('slow', function() {
            $("#form-horario-funcionamento").css('display', 'block');
        });
    } else {
        $("#form-horario-funcionamento").fadeOut('slow', function() {
            $("#form-horario-funcionamento").css('display', 'none');
        });
    }
});

$("#update-horario-funcionamento").submit(function() {
    var formData = {
        'id'                : $('#id').val(),
        'dias_config'       : $('#select-horario-funcionamento').val(),
        'aberto'            : $('#aberto').val(),
        'fechado'           : $('#fechado').val(),

        'dias' : {
            'dia0' : {
                'selecionado'   : $("#selecionado-dia0").val(),
                'dia'           : $("#dia-dia0").val(),
                'aberto'        : $("#aberto-dia0").val(),
                'fechado'       : $("#fechado-dia0").val()
            },
            'dia1' : {
                'selecionado'   : $("#selecionado-dia1").val(),
                'dia'           : $("#dia-dia1").val(),
                'aberto'        : $("#aberto-dia1").val(),
                'fechado'       : $("#fechado-dia1").val()
            },
            'dia2' : {
                'selecionado'   : $("#selecionado-dia2").val(),
                'dia'           : $("#dia-dia2").val(),
                'aberto'        : $("#aberto-dia2").val(),
                'fechado'       : $("#fechado-dia2").val()
            },
            'dia3' : {
                'selecionado'   : $("#selecionado-dia3").val(),
                'dia'           : $("#dia-dia3").val(),
                'aberto'        : $("#aberto-dia3").val(),
                'fechado'       : $("#fechado-dia3").val()
            },
            'dia4' : {
                'selecionado'   : $("#selecionado-dia4").val(),
                'dia'           : $("#dia-dia4").val(),
                'aberto'        : $("#aberto-dia4").val(),
                'fechado'       : $("#fechado-dia4").val()
            },
            'dia5' : {
                'selecionado'   : $("#selecionado-dia5").val(),
                'dia'           : $("#dia-dia5").val(),
                'aberto'        : $("#aberto-dia5").val(),
                'fechado'       : $("#fechado-dia5").val()
            },
            'dia6' : {
                'selecionado'   : $("#selecionado-dia6").val(),
                'dia'           : $("#dia-dia6").val(),
                'aberto'        : $("#aberto-dia6").val(),
                'fechado'       : $("#fechado-dia6").val()
            },
        }
    };

    $.ajax({
        url: "/lemenu/estabelecimento-update-horario", 
        data: formData,   
        type: "POST",                 
        success: function(data) {
            if (data == 1) {
                 $("#alert").fadeIn('slow', function() {
                    $("#alert").css('display', 'block');
                });
            } else {
                
            }
        }
    });

    return false;
});

$("#url_lemenu").change(function() {
    var url_lemenu = $("#url_lemenu").val();
    var id = $("#id").val();
    $.ajax({
        url: "/lemenu/validar-url-estabelecimento",    
        type: "POST",  
        data: {url_lemenu: url_lemenu, id:id},                    
        success: function(data) {
            if (data == 1) {
                $("#url_lemenu").css('border-color', 'red');
                $("#update_estabelecimento").prop('disabled', true);
            } else {
                $("#url_lemenu").css('border-color', 'green');
                $("#update_estabelecimento").prop('disabled', false);
            }
        }
    });
});

function set_data(nome, id){
    change_data(id);
    $("#"+nome.id).focusout(function() {
        location.reload();
    });

    $.ajax({
        url: "/lemenu/cardapio-update-item",    
        type: "POST",  
        data: {nome: nome.name, valor: nome.value, id:id},                    
        success: function(data) {
            if (data == true) {}
        }
    });
}

function set_data_cliente(nome, id){
    change_data(id);
    $("#"+nome.id).focusout(function() {
        location.reload();
    });

    $.ajax({
        url: "/lemenu/cliente-atualizar",    
        type: "POST",  
        data: {nome: nome.name, valor: nome.value, id:id},                    
        success: function(data) {
            if (data == true) {}
        }
    });
}

function delete_data(id){
    var nome       = $("#nome-"+id).val();  
    var preco      = $("#preco-"+id).val();  
    var categoria  = $("#select-data-"+id+" option:selected").val();
    var descricao  = $("#descricao-"+id).val();

    var view_text   = "<p>Se você selecionar a opção sim este registro será deletado do seu cardápio.</p>";
    var view_nome   = "<p><b>Nome</b>: "+nome+"</p>";
    var view_preco  = "<p><b>Preço</b>: "+preco+"</p>";
    var view_cat    = "<p><b>Categoria</b>: "+categoria+"</p>";
    var view_desc   = "<p><b>Descrição</b>: "+descricao+"</p>";
    var view_input  = "<input type=\"hidden\" name=\"id\" value=\""+id+"\">";
    var res         = view_text.concat(view_nome, view_preco, view_cat, view_desc, view_input);

    $("#modal-body-view").empty();
    $("#modal-body-view").append(res);
}

function edit_data(id){
    $(".display-msg").css({display:'none'});
    $.ajax({
        url: "/lemenu/exibir-cliente",    
        type: "POST",  
        data: {id:id},                    
        success: function(data) {
            data = JSON.parse(data);
            $("#modal_id").val(data.id);
            $("#modal_nome").val(data.nome);
            $("#modal_telefone").val(data.telefone);
            $("#modal_email").val(data.email);
            $("#modal_status option:contains('"+data.status+"')").attr('selected',true);
            $("#modal_site").val(data.site);
            $("#modal_plano option:contains('"+data.plano+"')").attr('selected',true);
            $("#modal_descricao").val(data.descricao);

        }
    });
}

$("#submit_update").click(function() {    
    var id          = $("#modal_id").val();
    var nome        = $("#modal_nome").val();
    var telefone    = $("#modal_telefone").val();
    var email       = $("#modal_email").val();
    var status      = $("#modal_status").val();
    var site        = $("#modal_site").val();
    var plano       = $("#modal_plano").val();
    var descricao   = $("#modal_descricao").val();
    $.ajax({
        url: "/lemenu/atualizar-cliente",    
        type: "POST",  
        data: {id:id, nome:nome,telefone:telefone,email:email,status:status,site:site,plano:plano,descricao:descricao},                    
        success: function(data) {
            $(".display-msg").css({display:'block'});
        }
    });   
});

function delete_cliente(id){
    var nome       = $("#nome-"+id).val();  
    var telefone   = $("#telefone-"+id).val();  
    var email      = $("#email-"+id).val();

    var view_text   = "<p>Se você selecionar a opção sim o cliente será deletado da base de dados.</p>";
    var view_nome   = "<p><b>Nome</b>: "+nome+"</p>";
    var view_tel    = "<p><b>Telefone</b>: "+telefone+"</p>";
    var view_email  = "<p><b>Email</b>: "+email+"</p>";
    var view_input  = "<input type=\"hidden\" name=\"id\" value=\""+id+"\">";
    var res         = view_text.concat(view_nome, view_tel, view_email, view_input);

    $("#modal-body-view").empty();
    $("#modal-body-view").append(res);
}

function isEmpty(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop))
        return false;
    }
    return true;
  }

function change_data(id){       
    var value = $("#select-data-"+id+" option:selected").val();
    if (value == 'nova') {
        $("td#select-"+id).empty();
        var input = '<input class="input-table" type="text" onchange="set_data(this, '+id+')" id="categoria" name="categoria" placeholder="Insira a nova categoria" value="">';
        $("td#select-"+id).append(input);
    }
}

function editImgModal(id, pasta){
     $.ajax({
        url: "/lemenu/cardapios/select_imagem",    
        type: "POST",  
        data: {id:id},                    
        success: function(data) {
            if (data.length == 2){
                var img = '/lemenu/app/assets/img/no-image.fw.png';
            } else {
                var img = '/lemenu/app/assets/img/clientes/'+pasta+'/original_'+data;
            }
            $('#previewing_edit').attr('src',img);
            $("#image").val(data);
            $("#id_image").val(id);
        }
    });
}

// upload image
$(document).ready(function (e) {
    $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
        $("#message").empty();
        $('#loading').show();
        $.ajax({
            url: "/lemenu/cardapio-upload",    // Url to which the request is send
            type: "POST",                      // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                $('#loading').hide();
                $("#message").html(data);
            }
    });
}));

// Function to preview image after validation
$(function() {
    $("#file").change(function() {
        $("#message").empty(); // To remove the previous error message
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
            $('#previewing').attr('src','/lemenu/app/assets/img/no-image-block.fw.png');
            $("#message").html("<p class=\"alert alert-danger\">Por favor, selecione um arquivo de imagem válido. Somente arquivos do tipo jpeg, jpg e png são permitidos.</p>");
            return false;
        } else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoaded(e) {
    $('#image_preview').css("display", "block");
    $('#previewing').attr('src', e.target.result);
    $('#previewing').attr('width', '250px');
    $('#previewing').attr('height', '230px');
    };
});

// upload for update image
$(document).ready(function (e) {
    $("#uploadimage_edit").on('submit',(function(e) {
        e.preventDefault();
        $("#messag_edit").empty();
        $('#loading_edit').show();
        var imagem = $("#image").val();
        var id = $("#id_image").val();
        $.ajax({
            url: "/lemenu/cardapios/upload/"+imagem+'/'+id,    // Url to which the request is send
            type: "POST",                      // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                $('#loading_edit').hide();
                $("#message_edit").html(data);
            }
    });
}));


// Function to preview image after validation
$(function() {
    $("#file_edit").change(function() {
        $("#message_edit").empty(); // To remove the previous error message
        var file = this.files[0];
        var imagefile = file.type;
        var match= ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
            $('#previewing_edit').attr('src','/lemenu/app/assets/img/no-image-block.fw.png');
            $("#message_edit").html("<p class=\"alert alert-danger\">Por favor, selecione um arquivo de imagem válido. Somente arquivos do tipo jpeg, jpg e png são permitidos.</p>");
            return false;
        } else {
            var reader = new FileReader();
            reader.onload = imageIsLoadedEdit;
            reader.readAsDataURL(this.files[0]);
        }
    });
});

function imageIsLoadedEdit(e) {
    $('#image_preview_edit').css("display", "block");
    $('#previewing_edit').attr('src', e.target.result);
    $('#previewing_edit').attr('width', '250px');
    $('#previewing_edit').attr('height', '230px');
    };
});