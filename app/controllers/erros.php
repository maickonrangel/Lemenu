<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: enderecos
*/

class Erros_Controller extends Controller_Core {

	function __construct(){
		parent::__construct();
		$this->meta_title = 'Página de Erro';
		$this->meta_description = 'Página de erros.';
		$this->meta_keywords = 'página, erros, avisos, Lemenu';
	}

	public function show($params = null){
		$erro = new Erros_Model;
		$avisos = $erro->aviso($params);
		require_once $this->render('show');
	}
}