<?php

class Painel_Controller extends Controller_Core{

	private $page;

	function __construct(){
		parent::__construct();
		$this->meta_title = 'Painel de Controle';
		$this->meta_description = 'Painel de controle do Lemenu.';
		$this->meta_keywords = 'login, painel, lemenu';
		$this->painel = new Painel_Model;
	}

	public function get_page(){
		return $this->page;
	}

	public function login($params = null){
		if (isset($_SESSION['status']) and $_SESSION['status'] == 's') {
			$this->redirect('admin');
		} else {
			$this->page = 'login';
			require_once $this->painel->render('login');
		}
	}

	public function logar(){
		if (isset($_REQUEST)) {
			$user = (new Usuarios_Model)->find_all_login(sha1($_REQUEST['senha']), $_REQUEST['email']);
			if (!empty($user[0]->nome)) {
				$estabelecimento 	= new Estabelecimentos_Model;
				$adress 			= new Enderecos_Model;
				@session_start();
				print_r($user);
				$_SESSION['id'] = $user[0]->id;
				if ($_SESSION['id_estabelecimento'] = $estabelecimento->find_by_column('id','id_usuario', $user[0]->id)) {
					$_SESSION['id_estabelecimento'] = $_SESSION['id_estabelecimento'][0]->id;
				}
				if ($_SESSION['id_endereco'] = $adress->find_by_column('id','id_estabelecimento', $_SESSION['id_estabelecimento'])) {
					$_SESSION['id_endereco'] = $_SESSION['id_endereco'][0]->id;
				}
				$_SESSION['nome'] 		= $user[0]->nome;
				$_SESSION['login'] 		= explode(' ',$user[0]->nome)[0];
				$_SESSION['email'] 		= $user[0]->email;
				$_SESSION['telefone'] 	= $user[0]->telefone;
				$_SESSION['status'] 	= $user[0]->status;
				$_SESSION['tipo'] 		= $user[0]->tipo;
				$_SESSION['data_cadastro'] = $user[0]->created_at;
				$this->redirect('admin');
			} else {
				$this->redirect('login-erro');
			}
		} else {
			$this->redirect();
		}
	}

	public function painel_admin(){
		$this->check_session();
		$this->page = 'painel';
		require_once $this->painel->render('painel_admin');
	}

	public function logout(){
		session_destroy();
		header("Location: ".URL_BASE.'login');
	}

	public function seus_dados($params = null){
		$this->check_session();
		global $_QUERY;
		$this->page = 'painel';
		require_once $this->painel->render('seus_dados');
	}

	public function seu_estabelecimento($params = null){
		$this->check_session();
		global $_QUERY;
		$this->page 		= 'painel';
		$bandeiras 			= (new Bandeiras_Model)->find_all_asc();
		$categorias 		= (new Categorias_Model)->find_all_asc();
		$estabelecimento 	= new Estabelecimentos_Model;
		$estabelecimentos 	= $estabelecimento->find_by_column('*', 'id', $_SESSION['id_estabelecimento']);
		$horario_funcionamento = unserialize($estabelecimentos[0]->horario_funcionamento);
		if (!empty($horario_funcionamento)) {
			$dias = $horario_funcionamento;
		} else {
			$dias = $estabelecimento->get_dias();
		}
		require_once $this->painel->render('seu_estabelecimento');
	}

	public function quadro_de_avisos($params = null){
		$this->check_session();
		global $_QUERY;
		$this->page = 'painel';
		$estabelecimentos = (new Estabelecimentos_Model)->find_by_column('id,descricao', 'id', $_SESSION['id_estabelecimento']);
		require_once $this->painel->render('quadro_de_avisos');
	}

	public function cardapios($params = null){
		$this->check_session();
		$cardapios 	= (new Cardapios_Model)->find_by_column('*', 'id_estabelecimentos', $_SESSION['id_estabelecimento']);
		$categorias = (new Cardapios_Model)->find_by_column_distinct('categoria', 'id_estabelecimentos', $_SESSION['id_estabelecimento']);
		$this->page = 'painel';
		require_once $this->painel->render('cardapios');
	}

	public function imprimir_cardapio(){
		$this->check_session();
		$this->page = 'painel';
		require_once $this->painel->render('imprimir_cardapio');
	}

	public function localizacao($params = null){
		$this->check_session();
		global $_QUERY;
		$this->page = 'painel';
		$endereco 	= (new Enderecos_Model)->find_by_column('*', 'id', $_SESSION['id_estabelecimento']);
		$paises 	= (new Paises_Model)->find_all();
		$estados 	= (new Estados_Model)->find_all();
		$cidades 	= (new Cidades_Model)->find_by_column('*', 'estado', $endereco[0]->estado);
		require_once $this->painel->render('localizacao');
	}

	public function alterar_senha(){
		$this->check_session();
		$this->page = 'painel';
		require_once $this->painel->render('alterar_senha');
	}

	public function notificacoes(){
		$this->check_session();
		$this->page = 'painel';
		require_once $this->painel->render('notificacoes');
	}

	public function upgrade_pro(){
		$this->check_session();
		$this->page = 'painel';
		require_once $this->painel->render('upgrade_pro');
	}

	public function servicos_cardapios(){
		$this->check_session();
		$this->page = 'painel';
		$servico = (new Servicos_Model)->__select('servicos','*','id_usuario='.$_SESSION['id']);
		require_once $this->painel->render('servicos_cardapios');
	}
	
	public function clientes(){
		$this->check_session();
		$clientes = (new Clientes_Model)->find_all();
		$this->page = 'painel';
		require_once $this->painel->render('clientes');
	}
}