<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: enderecos
*/

class Enderecos_Controller extends Controller_Core {

	public function index(){
		require_once $this->render('index');
	}
	
	public function create(){
		if (isset($_REQUEST) and isset($_SESSION['id'])) {
			$adress = new Enderecos_Model;			
			if($adress->save()){
				$_SESSION['id_endereco'] = $adress->find_by_column('id','id_estabelecimento', $_REQUEST['id_estabelecimento'])[0]->id;
				$this->redirect('admin');
			} else {
				$this->redirect('erro-endereco');
			}
		} else {
			$this->redirect();
		}
	}
	
	public function update(){
		$this->check_session();
		$endereco = new Enderecos_Model;
		if ($endereco->update()) {
			$this->redirect('localizacao?success=update');	
		} else {
			$this->redirect('localizacao?danger=erro');
		}
	}
	
	public function delete($table,$id){
		require_once $this->render('delete');
	}
	
}