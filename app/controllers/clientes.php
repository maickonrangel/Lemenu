<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: clientes
*/

class Clientes_Controller extends Controller_Core {

	public function create(){
		$this->check_session();
		$cliente = new Clientes_Model;
		if($cliente->save()){
			$this->redirect('clientes');
		} else {
			$this->redirect('cliente-erro-create');
		}
	}

	public function update($params = null){
		$this->check_session();
		if ($params != null and $params == 'update_modal') {
			$cliente = new Clientes_Model;
			if($cliente->update()){
				echo 'ok';
			} else {
				echo 'erro';
			}			
		} elseif ($params == null) {
			$stdClass = new StdClass;
			$nome 				= $_REQUEST['nome'];
			$stdClass->$nome 	= $_REQUEST['valor'];
			$stdClass->id 		= $_REQUEST['id'];

			$cliente = new Clientes_Model;
			if($cliente->__update('clientes', $stdClass)){
				$this->redirect('clientes');
			} else {
				$this->redirect('cliente-erro-update');
			}	
		}
	}

	public function delete(){
		$this->check_session();
		$cliente = new Clientes_Model;		
		if ($cliente->delete($_REQUEST['id'])) {
			$this->redirect('clientes');
		} else {
			$this->redirect('cliente-erro-delete');
		}
	}

	public function get_cliente(){
		$this->check_session();
		$cliente = (new Clientes_Model)->find_by_column('*', 'id', $_REQUEST['id']);
		echo json_encode($cliente[0]);
	}
}