<?php

class Templates_Controller extends Controller_Core{

	private $page;

	function __construct(){
		$this->meta_title = 'Template';
		$this->meta_description = 'Descricao.';
		$this->meta_keywords = 'Palavras chave';
	}

	public function template_1(){
		$estabelecimento = (new Estabelecimentos_Model)->find(1);
		$categorias = '';
		foreach ($estabelecimento[0]->categorias as $key => $value) {
			$categorias .= "{$value->nome} ";
		}
		
		$social_links = [
			'fa-facebook' 	=> $estabelecimento[0]->url_facebook,
			'fa-twitter'	=> $estabelecimento[0]->url_twitter,
			'fa-google-plus'=> $estabelecimento[0]->url_google_mais,
			'fa-instagram'	=> $estabelecimento[0]->url_instagran,
			'fa-youtube'	=> $estabelecimento[0]->url_youtube
		];

		$cardapio = new Cardapios_Model;
		$cardapios = $cardapio->find_by_column('*','id_estabelecimentos',$estabelecimento[0]->id);
		$cardapios_categorias = $cardapio->find_by_column_distinct('categoria', 'id_estabelecimentos',$estabelecimento[0]->id); 
		
		$menus = [
			['url'=>'#','menu'=>'Funcionamento']
		];

		$cardapio_por_catg = [];

		foreach ($cardapios_categorias as $key => $value) {
			$menus[] = ['url'=>'#','menu'=>$value->categoria];
			$catg_menu = $cardapio->find_filter("id_estabelecimentos={$estabelecimento[0]->id} AND categoria='{$value->categoria}'"); 
			$cardapio_por_catg[$value->categoria] = $catg_menu;
		}

		require_once $this->render('template-1', false);
	}

	public function template_2(){
		require_once $this->render('template-2', false);
	}

	public function template_3(){
		require_once $this->render('template-3', false);
	}

	public function background_template(){
		$background_img = new DirectoryIterator(PATH_BASE.'app/assets/img/background');
		$files = [];
		foreach ($background_img as $file_info) {
			if (($file_info->getFilename() != '.') and ($file_info->getFilename() != '..')) {
				$files[$file_info->getFilename()] = str_replace('_', ' ', ucfirst($file_info->getFilename()));
			}
		}
		echo json_encode($files);
	}
}