<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: estabelecimentos
*/

class Servicos_Controller extends Controller_Core {

	function __construct(){
		$this->meta_title = 'Serviços';
		$this->meta_description = 'Cadastrando dados do seu estabelecimeto no Lemenu.';
		$this->meta_keywords = 'Cadastro, estabelecimento, lemenu';
	}

	public function index(){
		require_once $this->render('index');
	}
	
	public function create(){
		if (isset($_REQUEST) and isset($_SESSION['id'])) {
			unset($_REQUEST['url']);
			$_REQUEST['id_usuario'] = $_SESSION['id'];
			$establishment = new Estabelecimentos_Model;
			if($establishment->__insert('estabelecimentos', $_REQUEST)){
				header("Location: ".URL_BASE.'abrir-conta-passo-3');
			} else {
				header("Location: ".URL_BASE.'erro-estabelecimento-passo-2');
			}
		} else {
			header("Location: ".URL_BASE);
		}
	}
}