<?php

class Home_Controller extends Controller_Core{

	function __construct(){
		$this->meta_title = 'Lemenu';
		$this->meta_description = 'Crie seus cardápios online.';
		$this->meta_keywords = 'Cadastro, nova conta, lemenu';
	}

	function index(){
		
		require_once $this->render('index');
	}

	function error(){
		echo 'erro';
	}
}