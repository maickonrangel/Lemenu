<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: usuarios
*/

class Usuarios_Controller extends Controller_Core {

	function __construct(){
		$this->meta_title = 'Criar Conta';
		$this->meta_description = 'Página de cadastro do Lemenu.';
		$this->meta_keywords = 'Cadastro, nova conta, lemenu';
	}

	public function index(){
		require_once $this->render('index');
	}
	
	public function create(){
		if (isset($_REQUEST)) {
			$_REQUEST['senha'] = sha1($_REQUEST['senha']);
			$user = new Usuarios_Model;
			if ($user->find_duplicate_by_email()) {
				$_SESSION['nome'] = $_REQUEST['nome'];
				$_SESSION['email'] = $_REQUEST['email'];
				$_SESSION['telefone'] = $_REQUEST['telefone'];
				$this->redirect('abrir-conta-erro-email');
			} else {
				if($user->save()){
					if (!file_exists(PATH_BASE.'app/assets/img/clientes/'.sha1($_REQUEST['email']))) {
						mkdir(PATH_BASE.'app/assets/img/clientes/'.sha1($_REQUEST['email']));
					}
					@session_start();
					$_SESSION['login'] 	= explode(' ',$_REQUEST['nome'])[0];
					$_SESSION['nome'] 	= $_REQUEST['nome'];
					$_SESSION['email'] 	= $_REQUEST['email'];
					$_SESSION['id'] 	= $user->find_last()[0]->id;
					$this->redirect('abrir-conta-passo-2');
				} else {
					$this->redirect('erro-usuario-passo-1');
				}
			}
		} else {
			$this->redirect();
		}
	}
	
	public function update(){
		$this->check_session();
		$usuario = new Usuarios_Model;
		if (isset($_REQUEST['senha'])) {
			$_REQUEST['senha'] = sha1($_REQUEST['senha']);
			if ($usuario->update()) {
				session_destroy();
				$this->redirect('login');
			}
		} elseif ($usuario->update()) {
			$_SESSION['nome']		= $_REQUEST['nome'];
			$_SESSION['email'] 		= $_REQUEST['email'];
			$_SESSION['telefone'] 	= $_REQUEST['telefone'];
			$this->redirect('usuario?success=update');
		} else {
			$this->redirect('usuario?danger=erro');
		}
	}
	
	public function delete($table,$id){
		require_once $this->render('delete');
	}

	public function new_user($params){
		if (isset($_SESSION['nome']) and isset($_SESSION['id']) and $_SESSION['id'] != 0) {
			$this->redirect('abrir-conta-passo-2');
		} else {
			$nome 		= isset($_SESSION['nome'])?$_SESSION['nome']:'';
			$email 		= isset($_SESSION['email'])?$_SESSION['email']:'';
			$telefone 	= isset($_SESSION['telefone'])?$_SESSION['telefone']:'';
			if ($params == 'passo 1') {
				$msg_title = 'Passo 1 - Por favor, insira seus dados.';
				$msg_desc = 'O botão de avançar só é liberado após todos os campos serem preenchidos.';
			} else {
				$msg_title = 'Erro - O e-mail informado já existe.';
				$msg_desc = 'Informe outo endereço de email. Caso esta conta seja sua, clique aqui <a href="#" class="btn btn-default btn-lg">Recuperar conta</a> para recuperá-la.';
			}
			require_once $this->render('new_user');
		}
	}
	
}