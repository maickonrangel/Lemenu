<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: cardapios
*/

class Cardapios_Controller extends Controller_Core {

	public function index(){
		require_once $this->render('index');
	}
	
	public function create(){
		$this->check_session();
		if (isset($_REQUEST)) {
			$cardapio = new Cardapios_Model;
			if ($cardapio->save()) {
				$this->redirect('cardapio');
			} else {
				$this->redirect('abrir-conta-passo-3');
			}
		}
	}
	
	public function select_imagem(){
		$this->check_session();
		$cardapio = (new Cardapios_Model)->find_by_column('imagem', 'id', $_REQUEST['id']);
		echo $cardapio[0]->imagem;
	}

	public function upload($parans = null){
		$this->check_session();
		$cardapio = new Cardapios_Model;
		$cardapio->formatar_imagem($parans);
	}

	public function update(){
		$this->check_session();
		$stdClass 			= new StdClass;
		$nome 				= $_REQUEST['nome'];
		$stdClass->$nome 	= $_REQUEST['valor'];
		$stdClass->id 		= $_REQUEST['id'];
		$cardapio 			= new Cardapios_Model;
		if ($cardapio->__update('cardapios', $stdClass)) {
			echo 'true';
		} else {
			echo 'false';
		}
	}
	
	public function delete(){
		$this->check_session();
		$cardapio = new Cardapios_Model;
		if ($cardapio->delete($_REQUEST['id'])) {
			$this->redirect('cardapio');
		} else {
			$this->redirect('cardapio-erro-delete');
		}
	}	
}