<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Controller: estabelecimentos
*/

class Estabelecimentos_Controller extends Controller_Core {

	function __construct(){
		$this->meta_title = 'Criar Conta - Passo 2';
		$this->meta_description = 'Cadastrando dados do seu estabelecimento no Lemenu.';
		$this->meta_keywords = 'Cadastro, estabelecimento, lemenu';
	}

	public function index(){
		require_once $this->render('index');
	}
	
	public function create(){
		if (isset($_REQUEST) and isset($_SESSION['id'])) {
			$_REQUEST['id_usuario'] = $_SESSION['id'];
			$_REQUEST['categoria'] = implode(",", $_REQUEST['categoria']);
			$estabelecimento = new Estabelecimentos_Model;
			if($estabelecimento->save()){
				$this->redirect('abrir-conta-passo-3');
			} else {
				$this->redirect('erro-estabelecimento-passo-2');
			}
		} else {
			$this->redirect();
		}
	}
	
	public function update(){
		$this->check_session();
		if (isset($_REQUEST['categoria']) and is_array($_REQUEST['categoria'])) {
			$_REQUEST['categoria'] = implode(",", $_REQUEST['categoria']);
		}
		
		if (isset($_REQUEST['formas_pagamento']) and is_array($_REQUEST['formas_pagamento'])) {
			$_REQUEST['formas_pagamento'] = implode(",", $_REQUEST['formas_pagamento']);
		}

		$_REQUEST['url_lemenu'] = preg_replace("/[^a-zA-Z0-9]/", "-", str_replace(' ', '-', $_REQUEST['url_lemenu']));
		
		$estabelecimento = new Estabelecimentos_Model;
		// $urls = $estabelecimento->find_like_by_all('url_lemenu');
		// $estabelecimento->append_urls($urls);
		// exit;

		if ($estabelecimento->update()) {
			if (isset($_REQUEST['descricao'])) {
				$this->redirect('quadro-de-avisos?success=update');
			} else {
				$this->redirect('estabelecimento?success=update');
			}
		}
	}
	
	public function update_horario_funcionamento(){
		$estabelecimento = new Estabelecimentos_Model;
		$_REQUEST['horario_funcionamento'] = $estabelecimento->horario_funcionamento_format();
		if ($estabelecimento->update()) {
			echo 1;
		} else {
			echo 0;
		}
	}

	public function delete($table,$id){
		require_once $this->render('delete');
	}
	
	public function new_stablishment($params){
		if (isset($_SESSION['id'])) {
			$estabelecimento = (new Estabelecimentos_Model)->__count('estabelecimentos','id_usuario='.$_SESSION['id']);
			if ($params == 'passo 3' or $estabelecimento == 1) {
				$this->redirect('abrir-conta-passo-3');
			} elseif ($params == 'passo 2' or $estabelecimento == 0) {
				$categorias = (new Categorias_Model)->find_all();
				$estabelecimento_model = new Estabelecimentos_Model;
				$avisos = $estabelecimento_model->aviso('passo-2');
				require_once $this->render('new_stablishment');
			} else {
				$this->redirect('abrir-conta');
			}
		} else {
			$this->redirect('abrir-conta');
		}
	}

	public function passo3(){
		if (isset($_SESSION['id'])) {
			if (isset($_SESSION['id_endereco']) and ($_SESSION['id_endereco'] > 0)) {
				$this->redirect('admin');
			} else {
				$paises = (new Paises_Model)->find_all();
				$estados = (new Estados_Model)->find_all();
				$estabelecimento_model = new Estabelecimentos_Model;
				$avisos = $estabelecimento_model->aviso('passo-3');
				$_SESSION['id_estabelecimento'] = $estabelecimento_model->find_by_column('id', 'id_usuario', $_SESSION['id'])[0]->id;	
				require_once $this->render('new_stablishment-step-3');
			}
		} else {
			$this->redirect('abrir-conta');
		}
	}

	public function get_city($state_id){
		$cities = (new Cidades_Model)->find_by_column('id,nome', 'estado', $state_id);
		echo json_encode($cities);
	}

	public function validate_url(){
		$this->check_session();
		$estabelecimento = new Estabelecimentos_Model;
		echo $estabelecimento->find_duplicate_by_url_lemenu($_REQUEST['url_lemenu']);
	}
	
	public function visualizacao_em_mapa(){
		$this->check_session();
		$endereco 			= (new Enderecos_Model)->find_by_column('*', 'id', $_SESSION['id_estabelecimento']);
		$filter_columns 	= 'nome, horario_funcionamento, categoria, formas_pagamento, url_lemenu, descricao';
		$estabelecimento 	= (new Estabelecimentos_Model)->find_by_column($filter_columns, 'id', $_SESSION['id']);

		$estabelecimento_mapa 				= new StdClass;
		$estabelecimento_mapa->pais 		= (new Paises_Model)->find_by_column('nome', 'id', $endereco[0]->pais)[0]->nome;
		$estabelecimento_mapa->cidade 		= (new Cidades_Model)->find_by_column('nome', 'id', $endereco[0]->cidade)[0]->nome;
		$estabelecimento_mapa->estado 		= (new Estados_Model)->find_by_column('nome', 'id', $endereco[0]->estado)[0]->nome;
		$estabelecimento_mapa->numero 		= $endereco[0]->numero;
		$estabelecimento_mapa->bairro 		= $endereco[0]->bairro;
		$estabelecimento_mapa->logradouro 	= $endereco[0]->logradouro;
		$estabelecimento_mapa->complemento 	= $endereco[0]->complemento;
		$estabelecimento_mapa->cep 			= $endereco[0]->cep;
		$estabelecimento_mapa->nome 		= $estabelecimento[0]->nome;
		
		$table = '
			 <table class="tabela-horarios">
                <thead>
                	<th>Dia</th>
                	<th>Abre as</th>
                	<th>Fecha as</th>
                </thead>
                <tbody>';
		foreach (unserialize($estabelecimento[0]->horario_funcionamento) as $key => $value) {
			if ($value['selecionado'] == 1) {
				$table .= '
					<tr>
	                	<td>'.$value['dia'].'</td>
	                	<td>'.$value['aberto'].'</td>
	                	<td>'.$value['fechado'].'</td>
	                </tr>
				';
			}
		}
       	$table .= '
                </tbody>
            </table>';

		$estabelecimento_mapa->horario_funcionamento = $table;

		$categorias 		= explode(',', $estabelecimento[0]->categoria);
		$categorias_total 	= '';
		
		foreach ($categorias as $key => $value) {
			$categoria 			= (new Categorias_Model)->find_by_column('nome', 'id', $value);
			$categorias_total  .= "{$categoria[0]->nome}, ";
		}
		
		$estabelecimento_mapa->categorias = $categorias_total;
		
		$formas_pagamentos 		= explode(',', $estabelecimento[0]->formas_pagamento);
		$formas_pagamento_total = '';
		
		foreach ($formas_pagamentos as $key => $value) {
			$formas_pagamento 		 = (new Bandeiras_Model)->find_by_column('nome, img', 'id', $value);
			$formas_pagamento_total .= '<img src="'.URL_BASE.'app/assets/img/bandeiras/'.$formas_pagamento[0]->img.'" class="bandeira-map" title="'.$formas_pagamento[0]->nome.'">';
		}
		
		$estabelecimento_mapa->formas_pagamentos	= "<div class=\"bandeiras-pagamento\">{$formas_pagamento_total}</div>";
		$estabelecimento_mapa->url_lemenu 			= $estabelecimento[0]->url_lemenu;
		$estabelecimento_mapa->descricao 			= $estabelecimento[0]->descricao;

		echo json_encode($estabelecimento_mapa);
	}
}