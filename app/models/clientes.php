<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: clientes
*/

class Clientes_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','nome','email','telefone','site','status','plano','descricao','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}
}