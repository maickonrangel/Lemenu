<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: cardapios
*/

class Cardapios_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','nome','preco','categoria','imagem','descricao','id_estabelecimento','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}

	public function formatar_imagem($update_img = null){
		if(isset($_FILES["file"]["type"])) {
		    $validextensions = array("jpeg", "jpg", "png");
		    $temporary = explode(".", $_FILES["file"]["name"]);
		    $file_extension = end($temporary);

		    //Approx. 100kb files can be uploaded.
		    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && 
		        ($_FILES["file"]["size"] < 500000) && in_array($file_extension, $validextensions)) {
		        if ($_FILES["file"]["error"] > 0) {
		            echo "<span class=\"alert alert-danger\">Erro ao carregar um arquivo: " . $_FILES["file"]["error"] . "</span><br/><br/>";
		        } else {
		            if (file_exists("upload/" . $_FILES["file"]["name"])) {
		                echo $_FILES["file"]["name"] . " <span class=\"alert alert-warning\"><b>já exise.</b></span> ";
		            } else {
		            	$new_name =  sha1($_FILES["file"]["name"]);
		                $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
		                $targetPath = PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email'])."/original_".$new_name.'.'.$file_extension; // Target path where file is to be stored
		                move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
		                $this->thumbnail_imagens($targetPath, $new_name);
		                
		                if ($update_img != null) {
		                	$path_file = PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email'])."/";
		                	if (file_exists($path_file.'original_'.$update_img[0])) {
		                		unlink($path_file.'original_'.$update_img[0]);
		                		unlink($path_file.'big_'.$update_img[0]);
		                		unlink($path_file.'large_'.$update_img[0]);
		                		unlink($path_file.'min_'.$update_img[0]);
		                		unlink($path_file.'small_'.$update_img[0]);
		                	}
		                	if (is_array($update_img)) {
		                		$_DATA = ['id'=>$update_img[1],'imagem'=>$new_name.'.'.$file_extension];
		                	} else {
		                		$_DATA = ['id'=>$update_img,'imagem'=>$new_name.'.'.$file_extension];
		                	}
		                	$this->__update('cardapios',$_DATA);
		                } else { ?>
		                <script type="text/javascript">
		                	var file_name = '<?php echo $new_name.'.'.$file_extension; ?>';
		                	$("#imagem_nome").val(file_name);
		                	$("#add_image").html("Imagem carregada");
		                </script>
		                <?php }
		                echo "<p class=\"alert alert-success\">Imagem carregada com sucesso!</p>";
		                echo "<b>Arquivo:</b> " . $_FILES["file"]["name"] . "<br>";
		                echo "<b>Tipo:</b> " . $_FILES["file"]["type"] . "<br>";
		                echo "<b>Tamanho:</b> " . intval($_FILES["file"]["size"] / 1024) . " kB<br>";
		            }
		        }
		    } else {
		    	if (!in_array($file_extension, $validextensions)) {
		    		echo "<p class=\"alert alert-danger modal-msg\">Imagem inválida para ser carregada. Esta imagem não possui uma extenssão permitida.<p>";
		    	} elseif (($_FILES["file"]["size"] > 500000)) {
		        	echo "<p class=\"alert alert-danger modal-msg\">Imagem inválida para ser carregada. Seu tamanho excede o limite permitido de 500kb. Esta imagem possui ".intval($_FILES["file"]["size"] / 1024)." kb<p>";
		        }
		    }
		}
	}

	public function thumbnail_imagens($caminho_arquivo_origem, $new_name) {
		$imagecreatefrom = '';
		$image = '';
		$temporary = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		switch ($file_extension) {
			case 'png': $imagecreatefrom = 'imagecreatefrompng'; $image = 'imagepng'; $quality = 9; $extension = '.png';
			break;
			case 'jpg': $imagecreatefrom = 'imagecreatefromjpeg'; $image = 'imagejpeg'; $quality = 75; $extension = '.jpg';
			break;
			case 'jpeg': $imagecreatefrom = 'imagecreatefromjpeg'; $image = 'imagejpeg'; $quality = 75; $extension = '.jpeg';
			break;
		}

		$this->thumbnail_min($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name);
		$this->thumbnail_small($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name);
		$this->thumbnail_large($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name);
		$this->thumbnail_big($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name);		
	}

	function thumbnail_min($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name){
		list($width, $height) = getimagesize($caminho_arquivo_origem);

		$min_img_width = 32;
		$min_img_height = 32;
		// create a min image 32x22
		$image_thumb = imagecreatetruecolor($min_img_width, $min_img_height);
		// transparence
		imagealphablending($image_thumb, false);
		imagesavealpha($image_thumb, true);
		$transparent = imagecolorallocatealpha($image_thumb, 255, 255, 255, 127);
		
		$min_image = $imagecreatefrom($caminho_arquivo_origem);
		imagecopyresampled($image_thumb, $min_image, 0, 0, 0, 0, $min_img_width, $min_img_height, $width, $height);
		$image($image_thumb, PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email']).'/min_'.$new_name.$extension, $quality);
		imagedestroy($image_thumb);
	}

	function thumbnail_small($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name){
		list($width, $height) = getimagesize($caminho_arquivo_origem);

		$small_img_width = 65;
		$small_img_height = 65;
		// create a small image 65x45
		$image_thumb = imagecreatetruecolor($small_img_width, $small_img_height);
		
		imagealphablending($image_thumb, false);
		imagesavealpha($image_thumb, true);
		$transparent = imagecolorallocatealpha($image_thumb, 255, 255, 255, 127);

		imagesavealpha($image_thumb, true);
		$small_image = $imagecreatefrom($caminho_arquivo_origem);	
		imagecopyresampled($image_thumb, $small_image, 0, 0, 0, 0, $small_img_width, $small_img_height, $width, $height);
		$image($image_thumb, PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email']).'/small_'.$new_name.$extension, $quality);
		imagedestroy($image_thumb);
	}

	function thumbnail_large($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name){
		list($width, $height) = getimagesize($caminho_arquivo_origem);

		$large_img_width = 145;
		$large_img_height = 145;
		// create a large image 145x105
		$image_thumb = imagecreatetruecolor($large_img_width, $large_img_height);
		
		imagealphablending($image_thumb, false);
		imagesavealpha($image_thumb, true);
		$transparent = imagecolorallocatealpha($image_thumb, 255, 255, 255, 127);

		imagesavealpha($image_thumb, true);
		$large_image = $imagecreatefrom($caminho_arquivo_origem);	
		imagecopyresampled($image_thumb, $large_image, 0, 0, 0, 0, $large_img_width, $large_img_height, $width, $height);
		$image($image_thumb, PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email']).'/large_'.$new_name.$extension, $quality);
		imagedestroy($image_thumb);
	}

	function thumbnail_big($imagecreatefrom, $image, $quality, $extension, $caminho_arquivo_origem, $new_name){
		list($width, $height) = getimagesize($caminho_arquivo_origem);

		$big_img_width = 200;
		$big_img_height = 200;
		// create a big image 200x130
		$image_thumb = imagecreatetruecolor($big_img_width, $big_img_height);

		imagealphablending($image_thumb, false);
		imagesavealpha($image_thumb, true);
		$transparent = imagecolorallocatealpha($image_thumb, 255, 255, 255, 127);

		imagesavealpha($image_thumb, true);
		$big_image = $imagecreatefrom($caminho_arquivo_origem);	
		imagecopyresampled($image_thumb, $big_image, 0, 0, 0, 0, $big_img_width, $big_img_height, $width, $height);
		$image($image_thumb, PATH_BASE."app/assets/img/clientes/".sha1($_SESSION['email']).'/big_'.$new_name.$extension, $quality);
		imagedestroy($image_thumb);
	}
}