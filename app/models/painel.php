<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: painel
*/

class Painel_Model extends Dbrecord_Core {

	function render($action, $layout = true){
		$this->action = $action;
		if ($layout == true && file_exists('app/views/painel/layout.phtml')) {
			return 'app/views/painel/layout.phtml';
		} else {
			return $this->content();
		}
	}

	function content(){
		$actual = get_class($this);
		$singleClassName = strtolower(str_replace("_Model", "", $actual));
		if (file_exists("app/views/{$singleClassName}/{$this->action}.phtml")) {
			return "app/views/{$singleClassName}/{$this->action}.phtml";
		}
	}
}