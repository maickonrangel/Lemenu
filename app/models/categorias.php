<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: cardapios
*/

class Categorias_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','nome'];
	}

	public function get_permit(){
		return $this->permit;
	}
}