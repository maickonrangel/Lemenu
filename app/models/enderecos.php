<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: enderecos
*/

class Enderecos_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','pais','cidade','estado','numero','bairro','logradouro','complemento','cep','id_estabelecimento','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}
}