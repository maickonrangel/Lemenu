<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: cardapios
*/

class Estados_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','nome','uf','pais'];
	}

	public function get_permit(){
		return $this->permit;
	}
}