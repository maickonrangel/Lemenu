<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: estabelecimentos
*/

class Erros_Model extends Dbrecord_Core {

	public function aviso($msg){
		$avisos = [
			'erro-endereco-create' => [
				'titulo' => 'Erro - Cadastrar endereço.',
				'desc' 	 => 'Um erro inesperado ocorreu no cadastro de endereço. Por favor contate o administrador.'
			],
			'erro-estabelecimento-passo-2-create' => [
				'titulo' => 'Erro - Cadastrar estabelecimento no passo 2.',
				'desc' 	 => 'Um erro inesperado ocorreu no cadastro de estabelecimento. Por favor contate o administrador.'
			],
			'erro-usuario-passo-1-create' => [
				'titulo' => 'Erro - Cadastrar usuário no passo 1.',
				'desc' 	 => 'Um erro inesperado ocorreu no cadastro de usuário. Por favor contate o administrador.'
			],
			'erro-cardapio-update' => [
				'titulo' => 'Erro - Atualizar item de cardápio.',
				'desc' 	 => 'Um erro inesperado ocorreu na atualização do cardápio. Por favor contate o administrador.'
			],
			'erro-cardapio-delete' => [
				'titulo' => 'Erro - Deletar item de cardápio.',
				'desc' 	 => 'Um erro inesperado ocorreu na exclusão de um item no cardápio. Por favor contate o administrador.'
			],
			'erro-404' => [
				'titulo' => 'Erro - 404.',
				'desc' 	 => 'Página não encontrada.'
			],
			'usuario-erro-update' => [
				'titulo' => 'Erro - Atualizar dados do usuário',
				'desc' 	 => 'Um erro inesperado ocorreu na atualização. Por favor contate o administrador.'
			],
			'erro-cliente' => [
				'titulo' => 'Erro - Cliente',
				'desc' 	 => 'Ocorreu um erro na página de cliente. Por favor contate o administrador.'
			]
		];
		return $avisos[$msg];
	}
}