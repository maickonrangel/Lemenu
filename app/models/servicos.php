<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: cardapios
*/

class Servicos_Model extends Dbrecord_Core {

	private $permit;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','descricao','id_usuario','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}
}