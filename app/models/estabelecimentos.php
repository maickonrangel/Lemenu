<?php
/*
	Classe gerada pelo Build_Core 
	@author Maickon Rangel
	Prodigio Framework - 2017
	Model: estabelecimentos
*/

class Estabelecimentos_Model extends Dbrecord_Core {

	private $permit;
	private $dias;

	public function __construct(){
		parent::__construct();
		$this->permit = ['id','id_usuario','nome','horario_funcionamento','categoria','formas_pagamento','url_lemenu','url_facebook','url_twitter','url_google_mais','url_instagran','url_youtube','descricao','created_at'];
	}

	public function get_permit(){
		return $this->permit;
	}

	public function horario_funcionamento_format(){
		$atendimento = [];
		if ($_REQUEST['dias_config'] == 1) {
			foreach ($this->get_dias() as $key => $value) {
				$atendimento[$key]['selecionado'] 	= $value['selecionado'];
				$atendimento[$key]['dia'] 			= $value['dia'];
				$atendimento[$key]['aberto'] 		= $_REQUEST['aberto'];
				$atendimento[$key]['fechado'] 		= $_REQUEST['fechado'];
			}
		} elseif ($_REQUEST['dias_config'] == 2) {
			foreach ($this->get_dias() as $key => $value) {
				if ($value['dia'] == 'Sábado' || $value['dia'] == 'Domingo') {
					$atendimento[$key]['selecionado'] 	= '0';
				} else {
					$atendimento[$key]['selecionado'] 	= '1';
				}
				$atendimento[$key]['dia']	 		= $value['dia'];
				$atendimento[$key]['aberto'] 		= $_REQUEST['aberto'];
				$atendimento[$key]['fechado'] 		= $_REQUEST['fechado'];
			}
		} else {
			$atendimento = $_REQUEST['dias'];
		}

		return serialize($atendimento);
	}

	public function aviso($passo){
		$avisos = [
			'passo-2' => [
				'titulo' => 'Passo 2 - '.$_SESSION['nome'].', agora vamos cadastrar o seu estabelecimento.',
				'desc' 	 => 'Os seus dados de usuário já foram cadastrado. Agora só falta preencher os dados do seu estabelecimento.'
			],
			'passo-3' => [
				'titulo' => 'Passo 3 - '.$_SESSION['nome'].', só falta cadastrarmos o endereço do seu estabelecimento.',
				'desc' 	 => 'Os seus novos clientes precisam conhecer o seu endereço para que possam te encontrar.'
			]
		];
		return $avisos[$passo];
	}

	public function append_urls($urls){
		$xml = "<?xml version=\"1.0\"?>\n<urls>\n";
		foreach ($urls as $key => $value) {
			$xml .= "\t<{$key}>{$value}</{$key}>\n";
		}
		$xml .= "</urls>";
		file_put_contents(PATH_BASE.'config/locale/'.$_SESSION['language'].'/urls.xml', $xml);
	}

	public function get_dias(){
		$dias = [
			'dia0' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Segunda Feira',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
			],

            'dia1' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Terça Feira',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ],

            'dia2' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Quarta Feira',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ],

            'dia3' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Quinta Feira',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ],

            'dia4' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Sexta Feira',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ],

            'dia5' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Sábado',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ],

            'dia6' => [
                'selecionado' 	=> '1',
                'dia' 			=> 'Domingo',
                'aberto' 		=> '08:00',
                'fechado' 		=> '22:00'
            ]
		];

		return $dias;
	}
}