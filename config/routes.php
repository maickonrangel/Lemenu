<?php
/*
	routes.php
	Crie url personalizadas atraves da configuracao abaixo

	name: Nome da rota
	controller: Nome do controler a ser usado
	Action: Nome do metodo a ser chamado pelo controller
*/

$_ROUTERS = [
	// exemplo
	// ['name'=>'nome_da_rota', 'controller'=>'nome_do_controller', 'action'=>'nome_da_action'],
	['name'=>'abrir-conta', 'controller'=>'usuarios', 'action'=>'new_user', 'params'=>'passo 1'],

	// url para cada passo-a-passo (abrindo conta)
	['name'=>'abrir-conta-passo-2', 'controller'=>'estabelecimentos', 'action'=>'new_stablishment', 'params'=>'passo 2'],
	['name'=>'abrir-conta-passo-3', 'controller'=>'estabelecimentos', 'action'=>'passo3'],

	// url para logins
	['name'=>'login', 		'controller'=>'painel', 'action'=>'login'],
	['name'=>'login-erro', 	'controller'=>'painel', 'action'=>'login', 'params'=>'erro'],
	['name'=>'logar', 		'controller'=>'painel', 'action'=>'logar'],
	
	// painel adm
	['name'=>'admin', 				'controller'=>'painel', 'action'=>'painel_admin'],
	['name'=>'usuario', 			'controller'=>'painel', 'action'=>'seus_dados'],
	['name'=>'estabelecimento', 	'controller'=>'painel', 'action'=>'seu_estabelecimento'],
	['name'=>'quadro-de-avisos', 	'controller'=>'painel', 'action'=>'quadro_de_avisos'],
	['name'=>'cardapio', 			'controller'=>'painel', 'action'=>'cardapios'],
	['name'=>'imprimir-cardapio', 	'controller'=>'painel', 'action'=>'imprimir_cardapio'],
	['name'=>'localizacao', 		'controller'=>'painel', 'action'=>'localizacao'],
	['name'=>'alterar-senha', 		'controller'=>'painel', 'action'=>'alterar_senha'],
	['name'=>'notificacoes', 		'controller'=>'painel', 'action'=>'notificacoes'],
	['name'=>'upgrade-pro', 		'controller'=>'painel', 'action'=>'upgrade_pro'],
	['name'=>'clientes', 			'controller'=>'painel', 'action'=>'clientes'],

	// erros
	['name'=>'erro-404', 						'controller'=>'erros', 	'action'=>'show', 	'params'=>'erro-404'],
	['name'=>'usuario-erro-update', 			'controller'=>'erros', 	'action'=>'show', 	'params'=>'usuario-erro-update'],
	['name'=>'erro-endereco', 					'controller'=>'erros', 	'action'=>'show', 	'params'=>'erro-endereco-create'],
	['name'=>'erro-estabelecimento-passo-2', 	'controller'=>'erros', 	'action'=>'show', 	'params'=>'erro-estabelecimento-passo-2-create'],
	['name'=>'erro-usuario-passo-1', 			'controller'=>'erros', 	'action'=>'show', 	'params'=>'erro-usuario-passo-1-create'],
	['name'=>'cardapio-erro-update', 			'controller'=>'erros', 	'action'=>'show',	'params'=>'erro-cardapio-update'],
	['name'=>'cardapio-erro-delete', 			'controller'=>'erros', 	'action'=>'show',	'params'=>'erro-cardapio-delete'],
	['name'=>'cliente-erro-create', 			'controller'=>'erros', 	'action'=>'show',	'params'=>'erro-cliente'],
	['name'=>'cliente-erro-update', 			'controller'=>'erros', 	'action'=>'show',	'params'=>'erro-cliente'],
	['name'=>'cliente-erro-delete', 			'controller'=>'erros', 	'action'=>'show',	'params'=>'erro-cliente'],
	
	// servicos
	['name'=>'servicos-cardapio', 	'controller'=>'painel', 'action'=>'servicos_cardapios'],
	
	// clientes
	['name'=>'novo-cliente', 		'controller'=>'clientes', 'action'=>'create'],
	['name'=>'deletar-cliente', 	'controller'=>'clientes', 'action'=>'delete'],
	['name'=>'cliente-atualizar', 	'controller'=>'clientes', 'action'=>'update'],
	['name'=>'exibir-cliente', 		'controller'=>'clientes', 'action'=>'get_cliente'],
	['name'=>'atualizar-cliente', 	'controller'=>'clientes', 'action'=>'update', 	'params'=>'update_modal'],
	
	// usuarios
	['name'=>'usuario-atualizar', 	'controller'=>'usuarios', 	'action'=>'update'],
	
	// endereos
	['name'=>'endereco-update', 	'controller'=>'enderecos', 	'action'=>'update'],
	
	// cardapios
	['name'=>'cardapio-upload', 			'controller'=>'cardapios', 	'action'=>'upload'],
	['name'=>'cardapio-update-item',		'controller'=>'cardapios', 	'action'=>'update'],
	['name'=>'cardapio-delete-item',		'controller'=>'cardapios', 	'action'=>'delete'],
	['name'=>'novo-item-cardapio', 			'controller'=>'cardapios', 	'action'=>'create'],
	['name'=>'cardapio-cadastrado', 		'controller'=>'painel', 	'action'=>'cardapios', 		'params'=>'cadastrado'],

	// estabelecimentos 
	['name'=>'estabelecimento-atualizar', 		'controller'=>'estabelecimentos', 	'action'=>'update'],
	['name'=>'estabelecimento-update-horario', 	'controller'=>'estabelecimentos', 	'action'=>'update_horario_funcionamento'],
	['name'=>'validar-url-estabelecimento', 	'controller'=>'estabelecimentos', 	'action'=>'validate_url'],
	['name'=>'estabelecimento-atualizado', 		'controller'=>'painel', 			'action'=>'seu_estabelecimento', 	'params'=>'atualizado'],
	['name'=>'estabelecimento-url-erro', 		'controller'=>'painel', 			'action'=>'seu_estabelecimento',	'params'=>'url-duplicada'],
	['name'=>'quadro-de-avisos-atualizado', 	'controller'=>'painel', 			'action'=>'quadro_de_avisos', 		'params'=>'atualizado'],
	['name'=>'maps', 							'controller'=>'estabelecimentos', 	'action'=>'visualizacao_em_mapa'],

	// logout - sair
	['name'=>'sair', 'controller'=>'painel', 'action'=>'logout'],

	// templates
	['name'=>'layout-cardapio', 	'controller'=>'templates', 'action'=>'template_1'],
	['name'=>'template-1', 			'controller'=>'templates', 'action'=>'template_1'],
	['name'=>'template-2', 			'controller'=>'templates', 'action'=>'template_2'],
	['name'=>'template-3', 			'controller'=>'templates', 'action'=>'template_3'],
	
	// url de erro de email invalido
	['name'=>'abrir-conta-erro-email', 'controller'=>'usuarios', 'action'=>'new_user', 'params'=>'erro-email']
];